-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-10-2020 a las 04:44:42
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbsistemahotelero`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `usuario_nombre` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_apellido` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_correo` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_contrasena` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_img` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_activo` tinyint(4) NOT NULL,
  `usuario_admin` tinyint(4) NOT NULL,
  `usuario_fecha_creacion` datetime NOT NULL,
  `usuario_fecha_modificacion` datetime NOT NULL,
  `usuario_creado` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_modificado` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_telefono` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `usuario_nombre`, `usuario_apellido`, `usuario_correo`, `usuario_contrasena`, `usuario_img`, `usuario_activo`, `usuario_admin`, `usuario_fecha_creacion`, `usuario_fecha_modificacion`, `usuario_creado`, `usuario_modificado`, `usuario_telefono`) VALUES
(18, 'intillay', 'Soto', 'intillay@soto.com', '1234', ' ', 1, 1, '2020-10-05 00:00:00', '2020-10-05 00:00:00', ' ', ' ', 741258963),
(19, 'intillay2', 'Soto2', 'intillay@soto.com2', '81dc9bdb52d04dc20036dbd8313ed055', ' ', 1, 1, '2020-10-05 00:00:00', '2020-10-05 00:00:00', ' ', ' ', 741258963);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `usuario_correo_UNIQUE` (`usuario_correo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
