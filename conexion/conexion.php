<?php

namespace sistemahotelero\conexion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of conexion
 *
 * @author christian
 */
class Conexion {

    private $conexion;

    function __construct() {
        $conexion = mysqli_connect("localhost", "root", "", "dbsistemahotelero");
        if (mysqli_connect_errno()) {
            printf("conexion fallida: %s\n", mysqli_connect_error());
            exit();
        }
        $this->conexion = $conexion;
    }

    public function getConexion() {

        return $this->conexion;
    }

}

/**
 * $conexion = new mysqli("localhost", "root", "oracle28", "dblacostanera");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
 */
       